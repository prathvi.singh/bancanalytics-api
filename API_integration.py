from flask import Flask, render_template, request
import calendar
import pandas as pd
import os

app =Flask(__name__)

first_amount = 0
flag = 0


@app.route('/t', methods=['POST'])
def main():
    # file = request.files["payloads"]
    content = request.json
    # print("-------------------")
    # data = pd.read_json(file)
    # print(data)
    
    df_transaction_data, df_account = data_fetch_dataframe(content)
    
    first_amount = calculation(df_transaction_data, df_account)
    df_eod = eod_test(df_transaction_data, first_amount)
    df_transaction_data = eod_column_main_dataframe(df_transaction_data)
    df_transaction_append_data = data_merge_transactions_no(df_transaction_data)

    json_df  = df_transaction_append_data.to_json(orient='records')
    response = app.response_class(
        response=json_df,
        status=200,
        mimetype='application/json')
    return  response

def data_fetch_dataframe(data):
    
    # print(data)
    transaction_data = data['Individual']['Global']['Bank']['Providers'][0]['Accounts'][0]['Transactions']
    account_data = data['Individual']['Global']['Bank']['Providers'][0]['Accounts']

    data_frame_all = pd.DataFrame(transaction_data)
    df_account = pd.DataFrame(account_data)

    df_transaction_data = data_frame_all[
        ['Date', 'Description', 'CategoryType', 'Category', 'Balance', 'Amount', 'MerchantName', 'TransactionId',
         'Type',
         'CategoryId', 'CategorizationKeyword']]

    return df_transaction_data, df_account
def calculation(df_transaction_data, df_account):
    df_transaction_data_date = df_transaction_data.sort_values(by='Date')
    first_amount = pd.to_numeric(df_account['OpeningBalance'][0])
    df_transaction_data['Balance'][len(df_transaction_data) - 1] = pd.to_numeric(df_account['OpeningBalance'])

    if (df_transaction_data['Type'][len(df_transaction_data) - 1] == 'Credit'):
        df_transaction_append_calc = df_transaction_data['Balance'][len(df_transaction_data) - 1] + \
                                     df_transaction_data['Amount'][len(df_transaction_data) - 1]
    else:
        df_transaction_append_calc = df_transaction_data['Balance'][len(df_transaction_data) - 1] - \
                                     df_transaction_data['Amount'][len(df_transaction_data) - 1]

    df_transaction_data['Balance'][len(df_transaction_data) - 1]= df_transaction_append_calc

    for i in range(len(df_transaction_data) - 2, -1, -1):
        df_transaction_data['Balance'][i] = df_transaction_append_calc

        if (df_transaction_data['Type'][i] == 'Credit'):
            df_transaction_append_calc = df_transaction_data['Balance'][i] + df_transaction_data['Amount'][i]
        else:
            df_transaction_append_calc = df_transaction_data['Balance'][i] - df_transaction_data['Amount'][i]

        df_transaction_data['Balance'][i] = df_transaction_append_calc

    return first_amount

def eod_test(df_transaction_data, first_amount):
    df_transaction_data['Date'] = pd.to_datetime(df_transaction_data['Date'], errors='coerce')

    df_transaction_date = df_transaction_data
    df_transaction_date['YearMonthAbrr'] = df_transaction_date['Date'].apply(lambda x: x.strftime('%B-%Y'))
    # data frame with only unique month and year abrr(Name of month) for column name
    df_transaction_date_month_year_abrr = df_transaction_date.groupby(['YearMonthAbrr']).groups.keys()
    # date frame with only unique month and year no for compaing month and year for calculation of eod
    df_transaction_date['YearMonth'] = df_transaction_date['Date'].apply(lambda x: x.strftime('%m-%Y'))
    df_transaction_date_month_year_numeric = sorted(df_transaction_date.groupby(['YearMonth']).groups.keys())
    df_transaction_date_days_eod = {}

    for date_month_year in df_transaction_date_month_year_numeric:

        split_year_month = date_month_year.split('-')

        no_of_days = calendar.monthrange(int(split_year_month[1]), int(split_year_month[0]))[1]

        df_transaction_data_date_year_month = df_transaction_date.query("YearMonth==@date_month_year")
        df_transaction_data_days_eod_day = {}
        first_transaction_day = 0
        df_transaction_data_days_eod_day = {}
        for row in df_transaction_data_date_year_month.iterrows():
            first_transaction_day = row[1]['Date'].day
            first_transaction_month = row[1]['Date'].month

        for row in df_transaction_data_date_year_month.iterrows():
            df_transaction_data_days_eod_day[row[1]['Date'].day] = row[1]['Balance']

        temp = 0

        for i in range(1, no_of_days + 1):
            if (i <= no_of_days):
                if (i < first_transaction_day):

                    df_transaction_data_days_eod_day[i] = first_amount
                    flag = 1
                elif (i in df_transaction_data_days_eod_day):
                    temp = df_transaction_data_days_eod_day[i]
                else:
                    df_transaction_data_days_eod_day[i] = temp
        first_amount = df_transaction_data_days_eod_day[no_of_days]

        df_transaction_date_days_eod[date_month_year] = df_transaction_data_days_eod_day

    df_eod = pd.DataFrame.from_dict(df_transaction_date_days_eod)
    df_eod.loc['avg'] = df_eod.mean()

    return df_eod


def eod_column_main_dataframe(df_transaction_data):
    df_transaction_merge = df_transaction_data
    df_transaction_merge['EOD'] = ""

    dataframe_date = df_transaction_merge

    # dataframe_final = dataframe_final.groupby(['Date']).min()
    dataframe_date = dataframe_date.drop_duplicates(subset=['Date'], keep='first')

    # dataframe_date = dataframe_date.drop_duplicates(keep='last')
    # dataframe_dates = dataframe_date['Date']

    dataframe_date['EOD'] = dataframe_date['Balance']

    # mergedStuff = pd.merge(df_transaction_merge, dataframe_date, on=['TransactionId'])
    mergedStuff = pd.merge(df_transaction_merge, dataframe_date[['TransactionId', 'EOD']], on='TransactionId',
                           how='left')

    mergedStuff = mergedStuff.drop(columns=['EOD_x'], axis=1)
    mergedStuff = mergedStuff.rename(columns={'EOD_y': 'EOD'})

    writer = pd.ExcelWriter('BankStatementtest.xlsx')
    dataframe_date.to_excel(writer, 'Transactions No')

    return mergedStuff



def data_merge_transactions_no(df_transaction_data):
    df_transaction_data_debit = df_transaction_data.query("Type=='Debit'")
    df_transaction_data_credit = df_transaction_data.query("Type=='Credit'")

    df_transaction_data_debit.rename(columns={'Amount': 'Debit'}, inplace=True)
    df_transaction_data_credit.rename(columns={'Amount': 'Credit'}, inplace=True)

    df_transaction_append_data = df_transaction_data_debit.append(df_transaction_data_credit, sort=True)
    df_transaction_append_data = df_transaction_append_data.sort_values(['Date'], ascending=[False])
    df_transaction_append_data['Date'] = df_transaction_append_data['Date'].apply(
        lambda x: x.strftime('%m/%d/%Y'))
    
    # print(df_transaction_append_data.to_json(orient='columns'))
    df_transaction_append_data = df_transaction_append_data.drop(columns=['CategoryId', 'CategorizationKeyword','Type'])
    df_transaction_append_data = df_transaction_append_data[
        ['Date', 'Description', 'CategoryType', 'Category', 'Credit', 'Debit', 'Balance', 'MerchantName',
         'TransactionId','EOD']]

    return df_transaction_append_data

if __name__ == '__main__':
    app.run()
    
